package com.isa.http;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.UUID;

import javax.net.ssl.HttpsURLConnection;

import com.isa.ssl.HTTPSCoder;
import com.isa.util.Arguments;

public class HttpAssist {
	private static final String TAG = "uploadFile";
	private static final int TIME_OUT = 10 * 10000000; // 超时时间
	private static final String CHARSET = "utf-8"; // 设置编码
	public static final String SUCCESS = "1";
	public static final String FAILURE = "0";

	public static String uploadFile(File file) throws Exception {
		String BOUNDARY = UUID.randomUUID().toString(); // 边界标识 随机生成
		String PREFIX = "--", LINE_END = "\r\n";
		String CONTENT_TYPE = "multipart/form-data"; // 内容类型
		String RequestURL = Arguments.WEB_SERVER_IP + "/servlet/UploadFile";
		URL url = new URL(RequestURL);
		
		//SSL认证
		HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
		HTTPSCoder.configSSLSocketFactory(conn);

		conn.setReadTimeout(TIME_OUT);
		conn.setConnectTimeout(TIME_OUT);
		conn.setDoInput(true); // 允许输入流
		conn.setDoOutput(true); // 允许输出流
		conn.setUseCaches(false); // 不允许使用缓存
		conn.setRequestMethod("POST"); // 请求方式
		conn.setRequestProperty("Charset", CHARSET); // 设置编码
		conn.setRequestProperty("connection", "keep-alive");
		conn.setRequestProperty("Content-Type", CONTENT_TYPE + ";boundary="
				+ BOUNDARY);
		if (file != null) {
			/**
			 * 当文件不为空，把文件包装并且上传
			 */
			OutputStream outputSteam = conn.getOutputStream();

			DataOutputStream dos = new DataOutputStream(outputSteam);
			StringBuffer sb = new StringBuffer();
			sb.append(PREFIX);
			sb.append(BOUNDARY);
			sb.append(LINE_END);
			/**
			 * 这里重点注意： name里面的值为服务器端需要key 只有这个key 才可以得到对应的文件
			 * filename是文件的名字，包含后缀名的 比如:abc.png
			 */

			sb.append("Content-Disposition: form-data; name=\"img\"; filename=\""
					+ file.getName() + "\"" + LINE_END);
			sb.append("Content-Type: application/octet-stream; charset="
					+ CHARSET + LINE_END);
			sb.append(LINE_END);
			dos.write(sb.toString().getBytes());
			InputStream is = new FileInputStream(file);
			byte[] bytes = new byte[1024];
			int len = 0;
			while ((len = is.read(bytes)) != -1) {
				dos.write(bytes, 0, len);
			}
			is.close();
			dos.write(LINE_END.getBytes());
			byte[] end_data = (PREFIX + BOUNDARY + PREFIX + LINE_END)
					.getBytes();
			dos.write(end_data);
			dos.flush();
			/**
			 * 获取响应码 200=成功 当响应成功，获取响应的流
			 */
			int res = conn.getResponseCode();
			if (res == 200) {
				return SUCCESS;
			}
		}
		return FAILURE;
	}
	public static File downLoad(String serverPath,String savedPath) throws Exception{  
              
            URL url=new URL(serverPath);  
            HttpsURLConnection conn=(HttpsURLConnection) url.openConnection();
            HTTPSCoder.configSSLSocketFactory(conn);
            conn.setRequestMethod("GET");  
            conn.setConnectTimeout(5000);  
            int code=conn.getResponseCode();  
            if(code==200){  
                //设置进度条的长度  
                InputStream is=conn.getInputStream();  
                File file = new File(savedPath);  
                FileOutputStream fileOutputStream=new FileOutputStream(file);  
                int len=0;  
                byte[] buffer= new byte[1024];  
                while((len=is.read(buffer))!=-1){  
                    fileOutputStream.write(buffer, 0,len);  
                }  
                fileOutputStream.flush();  
                fileOutputStream.close();  
                is.close();  
                return file;  
            }else {  
                return null;  
            }  
          
          
    }  
}
