package com.isa.util;

import com.isa.androidwebtrans.R;
import static com.isa.util.Arguments.*;
import com.isa.app.MyApp;

import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Toast;

public class Myhandler extends Handler {

	public static Myhandler myhandler = new Myhandler();

	public static Myhandler getInstance() {
		return myhandler;
	}

	private Myhandler() {
	}

	@Override
	public void handleMessage(Message msg) {
		switch (msg.what) {
		case MESSAGE_CONNECT_OK:
			
			Toast.makeText(MyApp.getContext(), (String) msg.obj,
					Toast.LENGTH_SHORT).show();
			break;
		case MESSAGE_CONNECT_BAD:
			Toast.makeText(MyApp.getContext(), "与服务器没有连接！", Toast.LENGTH_SHORT)
					.show();
			break;

		case MESSAGE_LOGIN_NETWRONG:
			Toast.makeText(MyApp.getContext(), "网络故障,登录失败！！",
					Toast.LENGTH_SHORT).show();
			break;
		case MESSAGE_LOGIN_OK:
			Toast.makeText(MyApp.getContext(), "登录成功！！", Toast.LENGTH_SHORT)
					.show();

			break;
		case MESSAGE_LOGIN_WRONG:
			Toast.makeText(MyApp.getContext(), "用户名或密码错误！！", Toast.LENGTH_SHORT)
					.show();
			break;
		case MESSAGE_UPLOAD_OK:
			Toast.makeText(MyApp.getContext(), "上传成功！！", Toast.LENGTH_SHORT)
					.show();
			break;
		case MESSAGE_UPLOAD_WRONG:
			Toast.makeText(MyApp.getContext(), "上传失败！！", Toast.LENGTH_SHORT)
					.show();
			break;
		case MESSAGE_EXTERNAL_UNUSE:
			Toast.makeText(MyApp.getContext(), "SD卡不可用！请重新设置！",
					Toast.LENGTH_SHORT).show();
			break;
		case MESSAGE_UPLOADKEY_OK:
			Toast.makeText(MyApp.getContext(), "上传密钥交换成功！", Toast.LENGTH_SHORT)
					.show();
			break;
		case MESSAGE_UPLOADKEY_WRONG:
			Toast.makeText(MyApp.getContext(), "上传密钥交换失败！", Toast.LENGTH_SHORT)
					.show();
			break;
		case MESSAGE_DOWNLOADLIST_WRONG:
			Toast.makeText(MyApp.getContext(), "获取下载目录失败！", Toast.LENGTH_SHORT)
					.show();
			break;
		case MESSAGE_DOWNLOAD_OK:
			Toast.makeText(MyApp.getContext(), "下载成功！", Toast.LENGTH_SHORT)
			.show();
			break;
		case MESSAGE_MANAGE_DELETE_OK:
			Toast.makeText(MyApp.getContext(), "删除服务器文件成功！", Toast.LENGTH_SHORT)
			.show();
			break;
		case MESSAGE_MANAGE_DELETE_WRONG:
			Toast.makeText(MyApp.getContext(), "删除服务器文件失败！", Toast.LENGTH_SHORT)
			.show();
			break;
		default:
			break;
		}
	}
}
