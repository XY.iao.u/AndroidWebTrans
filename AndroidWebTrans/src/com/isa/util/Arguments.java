package com.isa.util;

import android.os.Environment;

public class Arguments {
	public static final String WEB_SERVER_IP = "https://www.xiaoyuweb.com:8443/AndroidSafeWeb";
	public static final String WEB_LOGIN_SERVLET = WEB_SERVER_IP
			+ "/servlet/Login";
	public static final String WEB_KEY_CHANGE = WEB_SERVER_IP
			+ "/servlet/KeyChanger";
	public static final String WEB_DOWNLOAD = WEB_SERVER_IP
			+ "/servlet/DownloadFile";
	
	public static final String WEB_MANAGE = WEB_SERVER_IP+"/servlet/Manager";

	public static final int MESSAGE_CONNECT_OK = 0x123;
	public static final int MESSAGE_CONNECT_BAD = 0x124;
	public static final int MESSAGE_LOGIN_WRONG = 0x126;
	public static final int MESSAGE_LOGIN_OK = 0x125;
	public static final int MESSAGE_LOGIN_NETWRONG = 0x127;
	public static final int MESSAGE_UPLOAD_OK = 0x128;
	public static final int MESSAGE_UPLOAD_WRONG = 0x129;
	public static final int MESSAGE_EXTERNAL_UNUSE = 0x130;
	public static final int MESSAGE_UPLOADKEY_OK = 0x131;
	public static final int MESSAGE_UPLOADKEY_WRONG = 0x132;
	
	public static final int MESSAGE_DOWNLOADLIST_WRONG = 0x155;
	public static final int MESSAGE_DOWNLOAD_WRONG = 0x156;

	public static final int MESSAGE_MANAGE_DELETE_OK = 0x160;
	public static final int MESSAGE_MANAGE_DELETE_WRONG = 0x161;
	
	public static final String ROOT_PATH = Environment
			.getExternalStorageDirectory().getAbsolutePath()+"/SafeFile";

	public static final String PASSWORD = "123456";
	public static final int MESSAGE_DOWNLOAD_OK = 0x157;

	public static int IS_UPLOAD_KEY_CHANGED = 0;
	public static int IS_DOWNLOAD_KEY_CHANGED = 0;
}
