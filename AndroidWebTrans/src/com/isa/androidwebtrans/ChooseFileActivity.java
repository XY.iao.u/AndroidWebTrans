package com.isa.androidwebtrans;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.isa.http.HttpAssist;
import com.isa.util.Arguments;
import com.isa.util.Myhandler;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

public class ChooseFileActivity extends Activity {

	// @Override
	// protected void onCreate(Bundle savedInstanceState) {
	// super.onCreate(savedInstanceState);
	// setContentView(R.layout.activity_choose_file);
	//
	// }
	ListView listView;
	TextView textView;
	// 记录当前的父文件夹
	File currentParent;
	// 记录当前路径下的所有文件的文件数组
	File[] currentFiles;

	Myhandler handler;
	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_choose_file);
		
		handler = Myhandler.getInstance();
		listView = (ListView) findViewById(R.id.list);
		textView = (TextView) findViewById(R.id.path);
		// 获取系统的SD卡的目录
		File root = Environment.getExternalStorageDirectory();
		if (root.exists()) {
			currentParent = root;
			currentFiles = currentParent.listFiles();
			inflateListView(currentFiles);
		}
		Button btnFileReturn = (Button) findViewById(R.id.btnFileReturn);
		Button btnHome = (Button) findViewById(R.id.btnHome);
		Button btnFileCancel = (Button) findViewById(R.id.btnFileCancel);

		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				final int po =position;
				if (currentFiles[position].isFile()) {
					final View loginDialog = getLayoutInflater().inflate(
							R.layout.choose_file, null);
					((TextView) loginDialog.findViewById(R.id.tvFileName))
							.setText(currentFiles[position].getAbsolutePath());
					new AlertDialog.Builder(ChooseFileActivity.this)
							.setTitle("确认选择文件")
							.setView(loginDialog)
							.setPositiveButton("确定",
									new DialogInterface.OnClickListener() {

										@Override
										public void onClick(
												DialogInterface dialog,
												int which) {

											try {
												// 检查sd卡是否可以访问
												if (Environment
														.getExternalStorageState()
														.equals(Environment.MEDIA_MOUNTED)) {

													final File file = currentFiles[po];
															
													new Thread() {
														public void run() {
															try {
																HttpAssist
																		.uploadFile(file);
																handler.sendEmptyMessage(Arguments.MESSAGE_UPLOAD_OK);
															} catch (Exception e) {
																e.printStackTrace();
																handler.sendEmptyMessage(Arguments.MESSAGE_UPLOAD_WRONG);
															}
														};

													}.start();
												}
												else{
													handler.sendEmptyMessage(Arguments.MESSAGE_EXTERNAL_UNUSE);
												}
											} catch (Exception e) {
												handler.sendEmptyMessage(Arguments.MESSAGE_UPLOAD_WRONG);
											}

										}
									}).setNegativeButton("取消", null).show();
				} else {

					File[] tmp = currentFiles[position].listFiles();
					if (tmp == null || tmp.length == 0) {
						Toast.makeText(ChooseFileActivity.this,
								"Sorry!这个文件夹无法访问或者里面没有文件哦", Toast.LENGTH_SHORT)
								.show();

					} else {
						currentParent = currentFiles[position];
						currentFiles = tmp;
						inflateListView(tmp);
					}

				}
			}

		});
		btnFileReturn.setOnClickListener(new View.OnClickListener() {

			public void onClick(View v) {
				try {
					if (!currentParent.getCanonicalPath().equals("/")) {
						currentParent = currentParent.getParentFile();
						currentFiles = currentParent.listFiles();
						inflateListView(currentFiles);
					}
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		btnHome.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				try {
					currentParent = Environment.getExternalStorageDirectory();
					currentFiles = currentParent.listFiles();
					inflateListView(currentFiles);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		btnFileCancel.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				finish();
			}
		});

	}

	public void inflateListView(File[] files) {
		List<Map<String, Object>> fileList = new ArrayList<Map<String, Object>>();
		for (int i = 0; i < files.length; i++) {
			Map<String, Object> map = new HashMap<String, Object>();
			if (files[i].isFile()) {
				map.put("icon", R.drawable.file);
			} else {
				map.put("icon", R.drawable.folder);
			}
			map.put("path", files[i].getName());
			fileList.add(map);
		}
		SimpleAdapter adapter = new SimpleAdapter(this, fileList,
				R.layout.line, new String[] { "icon", "path" }, new int[] {
						R.id.icon, R.id.file_name });
		listView.setAdapter(adapter);
		try {
			textView.setText("当前路径为：" + currentParent.getCanonicalPath());
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
