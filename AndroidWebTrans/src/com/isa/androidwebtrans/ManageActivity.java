package com.isa.androidwebtrans;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.lang.*;

import com.isa.http.HttpClientTest;
import com.isa.util.Arguments;
import com.isa.util.Myhandler;

import android.R.array;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

public class ManageActivity extends Activity {

	private Button btnTop;
	private Button btnSuffix;
	private Button btnFileSize;
	private Button btnDate;
	public List<String> list;
	private String response;
	private ListView lv;
	private Myhandler myhandler = Myhandler.getInstance();
	public static boolean SIZE_FLAG = true;
	public OnClickListener btnClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {

			list = null;

			switch (v.getId()) {
			case R.id.btnTop:
				new Thread() {
					@Override
					public void run() {
						response = getDownList();
					}
				}.start();
				try {
					Thread.sleep(500);
				} catch (InterruptedException e) {
				}
				list = getTopList(response);
				break;
			case R.id.btnSuffix:
				new Thread() {
					@Override
					public void run() {
						response = getDownList();
					}
				}.start();
				try {
					Thread.sleep(500);
				} catch (InterruptedException e) {
				}
				list = getSuffixList(response);
				break;

			case R.id.btnFileSize:
				new Thread() {
					@Override
					public void run() {
						response = getFileSizeList();
					}
				}.start();
				try {
					Thread.sleep(500);
				} catch (InterruptedException e) {
				}
				list = getTopList(response);
				break;
			case R.id.btnDate:
				new Thread() {
					@Override
					public void run() {
						// response = getDateList();
					}
				}.start();
				try {
					Thread.sleep(500);
				} catch (InterruptedException e) {
				}
				list = getTopList(response);

				break;

			default:
				break;
			}

			lv.setAdapter(new ArrayAdapter(ManageActivity.this,
					android.R.layout.simple_list_item_1, list));

		}
	};

	// 获取从小到大或者从大到小的文件序列
	private String getFileSizeList() {
		String result = null;
		try {
			if (SIZE_FLAG)
				result = HttpClientTest.sendGet(Arguments.WEB_MANAGE,
						"action=getbiglist");
			else
				result = HttpClientTest.sendGet(Arguments.WEB_MANAGE,
						"action=getsmalllist");
			SIZE_FLAG = !SIZE_FLAG;
		} catch (Exception e) {
			e.printStackTrace();
			myhandler.sendEmptyMessage(Arguments.MESSAGE_DOWNLOADLIST_WRONG);
		}
		return result;

	}

	// 按照字母排序
	private List<String> getTopList(String response) {
		List<String> topList = new ArrayList<String>();
		if (response != null && response.length() != 0) {
			String[] str = response.split(",");
			for (String s : str) {
				topList.add(s);
			}
		}

		return topList;
	}

	private List<String> getSuffixList(String response) {
		List<String> suffixList = new ArrayList<String>();
		// 获取所有的后缀名
		Set<String> suffixs = new HashSet<String>();

		if (response != null && response.length() != 0) {

			for (String str : response.split(",")) {
				String[] strs = str.split("\\.");
				if (strs.length > 0)
					suffixs.add(strs[strs.length - 1]);
			}
		}

		List<String> all = Arrays.asList(response.split(","));
		for (String s : suffixs) {
			for (String su : all) {
				if (su.endsWith(s)) {
					suffixList.add(su);
				}
			}
		}

		return suffixList;
	}

	private String getDownList() {
		String result = null;
		try {
			result = HttpClientTest.sendGet(Arguments.WEB_DOWNLOAD,
					"action=getfilelist");

		} catch (Exception e) {
			e.printStackTrace();
			myhandler.sendEmptyMessage(Arguments.MESSAGE_DOWNLOADLIST_WRONG);
		}
		return result;

	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_manage_list);

		btnTop = (Button) findViewById(R.id.btnTop);
		btnSuffix = (Button) findViewById(R.id.btnSuffix);
		btnFileSize = (Button) findViewById(R.id.btnFileSize);
		btnDate = (Button) findViewById(R.id.btnDate);

		btnDate.setOnClickListener(btnClickListener);
		btnTop.setOnClickListener(btnClickListener);
		btnSuffix.setOnClickListener(btnClickListener);
		btnFileSize.setOnClickListener(btnClickListener);

		lv = (ListView) findViewById(R.id.lvManage);
		new Thread() {

			@Override
			public void run() {
				response = getDownList();
			}
		}.start();
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
		}
		list = getTopList(response);
		lv.setAdapter(new ArrayAdapter(ManageActivity.this,
				android.R.layout.simple_list_item_1, list));
		// 长按删除
		lv.setOnItemLongClickListener(new OnItemLongClickListener() {

			@Override
			public boolean onItemLongClick(AdapterView<?> parent, View view,
					int position, long id) {
				final int _position = position;
				final String fileName = list.get(position);
				new AlertDialog.Builder(ManageActivity.this)
						.setTitle("确认框")
						.setMessage("确定要删除该文件吗？")
						.setPositiveButton("删除",
								new DialogInterface.OnClickListener() {

									@Override
									public void onClick(DialogInterface dialog,
											int which) {

										new Thread() {
											public void run() {
												try {
													HttpClientTest
															.sendGet(
																	Arguments.WEB_MANAGE,
																	"action=delete&file="
																			+ fileName);
													list.remove(_position);

													myhandler
															.sendEmptyMessage(Arguments.MESSAGE_MANAGE_DELETE_OK);
												} catch (Exception e) {
													e.printStackTrace();
													myhandler
															.sendEmptyMessage(Arguments.MESSAGE_MANAGE_DELETE_WRONG);
												}
											};

										}.start();
										try {
											Thread.sleep(500);
										} catch (InterruptedException e) {
										}

										lv.setAdapter(new ArrayAdapter(
												ManageActivity.this,
												android.R.layout.simple_list_item_1,
												list));

									}
								}).setNegativeButton("取消", null).show();

				return true;
			}
		});

	}
}
