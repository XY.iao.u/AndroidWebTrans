package com.isa.crypto;

import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;

import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;

public class Crypto {
	private static final String TAG = "Crypto";

	public Crypto() {

	}

	public static SecretKey generateKey(String keyType) {
		SecretKey sKey = null;
		try {
			KeyGenerator keyGenerator = KeyGenerator.getInstance(keyType);
			sKey = keyGenerator.generateKey();
		} catch (NoSuchAlgorithmException e) {
			System.out.println(TAG + ":No such algorithm exception");
		}
		return sKey;
	}

	public static KeyPair generateKeyPair(String keyType) {
		KeyPair keyPair = null;
		try {
			KeyPairGenerator kpg = KeyPairGenerator.getInstance(keyType);
			kpg.initialize(1024);
			keyPair = kpg.genKeyPair();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return keyPair;

	}

}
