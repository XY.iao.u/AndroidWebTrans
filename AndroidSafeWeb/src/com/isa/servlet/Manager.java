package com.isa.servlet;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.isa.util.MyFileFilter;

public class Manager extends HttpServlet {

	/**
	 * Constructor of the object.
	 */
	public Manager() {
		super();
	}
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		if (request.getParameter("action") != null
				&& request.getParameter("action").equals("getbiglist")) {
			File root = new File(this.getServletContext().getRealPath(
					"/WEB-INF/upload"));
			
			File[] files = root.listFiles(new MyFileFilter("tmp"));
			List<File> filelist = Arrays.asList(files);
			Collections.sort(filelist, new Comparator<File>() {
				public int compare(File f1, File f2) {
					long diff = f1.length() - f2.length();
					if (diff > 0)
						return 1;
					else if (diff == 0)
						return 0;
					else
						return -1;
				}

				public boolean equals(Object obj) {
					return true;
				}
			});
			StringBuilder sb = new StringBuilder("");
			if (filelist != null)
				for (File f : filelist) {
					sb.append(f.getName() + ",");
				}
			if (sb.length() != 0) {
				sb.substring(0, sb.length() - 1);
			}
			System.out.println(sb.toString());
			PrintWriter out = response.getWriter();
			out.write(sb.toString());
			out.flush();
			out.close();

			
		} else if (request.getParameter("action") != null
				&& request.getParameter("action").equals("getsmalllist")) {
			File root = new File(this.getServletContext().getRealPath(
					"/WEB-INF/upload"));
			
			File[] files = root.listFiles(new MyFileFilter("tmp"));
			List<File> filelist = Arrays.asList(files);
			Collections.sort(filelist, new Comparator<File>() {
				public int compare(File f1, File f2) {
					long diff = f1.length() - f2.length();
					if (diff > 0)
						return -1;
					else if (diff == 0)
						return 0;
					else
						return 1;
				}

				public boolean equals(Object obj) {
					return true;
				}
			});
			StringBuilder sb = new StringBuilder("");
			if (filelist != null)
				for (File f : filelist) {
					sb.append(f.getName() + ",");
				}
			if (sb.length() != 0) {
				sb.substring(0, sb.length() - 1);
			}
			System.out.println(sb.toString());
			PrintWriter out = response.getWriter();
			out.write(sb.toString());
			out.flush();
			out.close();

			
		}
		//删除文件
		else if(request.getParameter("action").equals("delete")){
			request.setCharacterEncoding("utf-8");
			String fileName = request.getParameter("file");
			System.out.println("\n"+fileName+"\n");
			File file = new File(this.getServletContext().getRealPath(
					"/WEB-INF/upload/"+fileName));
			if(file!=null&&file.exists()){
				file.delete();
			}
			
		}
	}
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		doGet(request, response);
	}

	/**
	 * Initialization of the servlet. <br>
	 * 
	 * @throws ServletException
	 *             if an error occurs
	 */
	public void init() throws ServletException {
		// Put your code here
	}

}
