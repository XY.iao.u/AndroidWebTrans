package com.isa.util;

import java.io.File;
import java.io.FileFilter;
import java.io.FilenameFilter;

public class MyFileFilter implements FilenameFilter{
	public String filName;
	
	public MyFileFilter(String filName){
		this.filName = filName;
	}
	@Override
	public boolean accept(File arg0, String arg1) {
		
		return !arg1.endsWith(filName);
	}


}
